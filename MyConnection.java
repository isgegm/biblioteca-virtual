import java.sql.*;
public class MyConnection {
	Connection con;
	public Connection getConnection(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/poo", "root","");
			System.out.println("Connected");
		}catch(SQLException e){
			System.out.println("Connection error: "+ e);
		}
		catch(ClassNotFoundException e){
			System.out.println("Connection error: "+ e);
		}
		return con;
	}
}